import React, { Component } from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";
import Monitor from "../components/monitor/Monitor";
import { connect } from "react-redux";
import { productsFetch } from "../action";

class Home extends Component {


  constructor(props){
    super(props);
    
  }
  componentDidMount(){
    this.props.productsFetch();
  }

  render(){
    return(
      <div>
        <title>{"Healty cafe"}</title>
      <Header />
      <Monitor products ={this.props.products} />
      <Footer company = "APPLE" email = "App@apple.com" />
    </div>
    );
  }
}

function mapStateToProps(state){
  return {products : state.products};

}

export default connect(mapStateToProps, {productsFetch})(Home); 
