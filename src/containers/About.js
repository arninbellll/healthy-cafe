import React from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";

const About = () => {
    return (
        <div>
            <Header />
            <div className = "container col-md-5">
                <h3>สวัสดีค่ะ แอนค่ะ แอนค่ะ </h3>
                <p className = "title text-justify mt-4 mb-4">
                    เราคือร้านอาหาร ที่เน้นอาหารที่อร่อยจริงๆเท่านั้น ไม่ให้ความสำคัญกับสุขภาพเท่าไร 
                    เพราะสุขภาพที่ดีนั้นคุณสามารถส้รางได้ด้วย "ออกกำลังกาย"
                    ดังนั้นกินของอร่อยก่อน แล้วคุณจะมีกำลังไปทำในสิ่งที่คุณรัก
                </p>
                <h4 className ="text-success"> หนูชื่อแอน มาจาก เฮลตี้คาเฟ่ </h4>
            </div>
            <Footer company ="APPLE" email = "apple@apple.com"/>
        </div>
    )
}
export default About;